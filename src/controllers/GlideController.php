<?php

namespace werewolf8904\glide\controllers;

use werewolf8904\glide\actions\GlideAction;
use werewolf8904\glide\components\IGlide;
use Yii;
use yii\di\Instance;
use yii\web\Controller;

/**
 * @author Eugene Terentev <eugene@terentev.net>
 * With this controller you can create a simple
 * configurations like that @see https://github.com/trntv/yii2-starter-kit/blob/master/storage/index.php
 */
class GlideController extends Controller
{
    public function actions()
    {
        return [
            'index' => [
                'class' => GlideAction::class
            ]
        ];
    }

    /**
     * @return mixed
     * @throws \yii\base\InvalidConfigException
     * @throws \yii\di\NotInstantiableException
     */
    public function actionError()
    {
        $glide = Instance::ensure(IGlide::class);
        $glide->signKey = null;
        return Yii::createObject(\trntv\glide\actions\GlideAction::class, ['error', $this])
            ->runWithParams(['path' => $glide->getNoImage()]);

    }
}
