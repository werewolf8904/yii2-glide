<?php
/**
 * Created by PhpStorm.
 * User: werewolf
 * Date: 09.08.2018
 * Time: 14:23
 */

namespace werewolf8904\glide\components;


use yii\base\InvalidConfigException;

/**
 * @author Eugene Terentev <eugene@terentev.net>
 *
 * @param $source        \League\Flysystem\FilesystemInterface
 * @param $cache         \League\Flysystem\FilesystemInterface
 * @param $server        \League\Glide\Server
 * @param $httpSignature \League\Glide\Signatures\Signature
 * @param $urlBuilder    \League\Glide\Urls\UrlBuilderFactory
 */
interface IGlide
{
    public function getImageSourceUrl($image);

    /**
     * @param array $params
     * @param bool  $scheme
     *
     * @return bool|string
     * @throws InvalidConfigException
     */
    public function createSignedUrl(array $params, $scheme = false);

    public function getGlideThumbnail($image, $preset = null, array $params = []): string;

    /**
     * Get configured server.
     *
     * @return \League\Glide\Server
     */
    public function getServer();

    /**
     * Get noimage placeholder
     *
     * @return string
     */
    public function getNoImage();
}