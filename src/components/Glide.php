<?php

namespace werewolf8904\glide\components;

use League\Glide\Server;
use yii\base\InvalidConfigException;
use yii\helpers\ArrayHelper;


/**
 * @author Eugene Terentev <eugene@terentev.net>
 *
 * @param $source        \League\Flysystem\FilesystemInterface
 * @param $cache         \League\Flysystem\FilesystemInterface
 * @param $server        \League\Glide\Server
 * @param $httpSignature \League\Glide\Signatures\Signature
 * @param $urlBuilder    \League\Glide\Urls\UrlBuilderFactory
 */
class Glide extends \trntv\glide\components\Glide implements IGlide
{

    /**
     * Whether to cache with file extensions.
     *
     * @var bool
     */
    public $cacheWithFileExtensions = true;

    public $withTimeStamp = false;

    public $default_create_signed_config = ['glide/index'];

    public $no_image = 'no_image_2.png';

    public $source_url;

    public function getImageSourceUrl($image)
    {
        return $this->source_url . $image;
    }

    public function getNoImage()
    {
        return $this->no_image;
    }

    /**
     * @param       $image
     * @param null  $preset
     * @param array $params
     *
     * @return string
     * @throws \yii\base\InvalidConfigException
     * @throws \League\Flysystem\FileNotFoundException
     */
    public function getGlideThumbnail($image, $preset = null, array $params = []): string
    {
        if (!$image) {
            $image = $this->no_image;
        }
        $config = $this->default_create_signed_config;
        $config['path'] = $image;
        if ($preset) {
            $config['p'] = $preset;
        }

        if (!empty($params)) {
            $config = ArrayHelper::merge($config, $params);
        }
        return $this->createSignedUrl($config, true);
    }

    /**
     * @param array $params
     * @param bool  $scheme
     *
     * @return bool|string
     * @throws InvalidConfigException
     * @throws \League\Flysystem\FileNotFoundException
     */
    public function createSignedUrl(array $params, $scheme = false)
    {
        $path = ArrayHelper::remove($params, 'path') ?: $this->no_image;
        $route = ArrayHelper::remove($params, 0);

        if ($this->withTimeStamp) {
            $timestamp = $this->getServer()->getSource()->getTimestamp($path);
            if ($timestamp) {
                $params['v'] = $timestamp;
            }
        }

        $params['path'] = $this->getServer()->getCachePath($path, $params);
        $params[0] = $route;

        return parent::createSignedUrl($params, $scheme);
    }

    /**
     * Get configured server.
     *
     * @return \League\Glide\Server
     */
    public function getServer()
    {
        if (!$this->server) {
            $server = new Server(
                $this->getSource(),
                $this->getCache(),
                $this->getApi()
            );

            $server->setSourcePathPrefix($this->sourcePathPrefix);
            $server->setCachePathPrefix($this->cachePathPrefix);
            $server->setGroupCacheInFolders($this->groupCacheInFolders);
            $server->setDefaults($this->defaults);
            $server->setPresets($this->presets);
            $server->setBaseUrl($this->baseUrl);
            $server->setResponseFactory($this->responseFactory);

            $this->server = $server;
            $this->server->setCacheWithFileExtensions($this->cacheWithFileExtensions);
        }
        return $this->server;
    }
}
