<?php
return [
    'container' => [
        'singletons' => [
            \werewolf8904\glide\components\IGlide::class => [
                'class' => \werewolf8904\glide\components\Glide::class,
                'sourcePath' => '@storage/web/source',
                'cachePath' => '@storage/web/cache',
                'urlManager' => 'urlManagerStorage',
                'maxImageSize' => $params['glide.maxImageSize'],
                'signKey' => $params['glide.signKey'],
                'no_image' => $params['glide.no_image'],
            ],
        ],
    ],
    'components' => [
        'glide' => \werewolf8904\glide\components\IGlide::class,
    ]
];
