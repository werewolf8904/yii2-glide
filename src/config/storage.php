<?php
return [
    'controllerMap' => [
        'glide' => \werewolf8904\glide\controllers\GlideController::class
    ],
    'components' => [
        'urlManager' => [
            'class' => yii\web\UrlManager::class,
            'enablePrettyUrl' => true,
            'showScriptName' => false,
            'rules' => [
                'glide' => ['pattern' => 'cache/<path:(.*)>', 'route' => 'glide/index', 'encodeParams' => false]
            ]
        ],
        'errorHandler' => [
            'errorAction' => 'glide/error'
        ],
    ],
];
