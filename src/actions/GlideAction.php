<?php

namespace werewolf8904\glide\actions;

use Symfony\Component\HttpFoundation\Request;
use Yii;
use yii\base\NotSupportedException;
use yii\web\BadRequestHttpException;
use yii\web\NotFoundHttpException;
use yii\web\Response;

/**
 * @author Eugene Terentev <eugene@terentev.net>
 */
class GlideAction extends \trntv\glide\actions\GlideAction
{
    /**
     * @param $path
     *
     * @return \yii\console\Response|\yii\web\Response
     * @throws \yii\base\InvalidConfigException
     * @throws \yii\base\NotSupportedException
     * @throws \yii\web\BadRequestHttpException
     * @throws \yii\web\NotFoundHttpException
     */
    public function run($path)
    {
        unset($_GET['path']);
        $server = $this->getServer();
        $path = \dirname($path);
        if (!$server->sourceFileExists($path)) {
            throw new NotFoundHttpException;
        }

        if ($this->getComponent()->signKey) {
            $request = Request::create(Yii::$app->request->getUrl());
            if (!$this->validateRequest($request)) {
                throw new BadRequestHttpException('Wrong signature');
            }
        }

        try {
            $response = Yii::$app->response;
            $response->format = Response::FORMAT_RAW;
            $path = $server->makeImage($path, Yii::$app->request->get());
            $response->headers->add('Content-Type', $server->getCache()->getMimetype($path));
            $response->headers->add('Cache-Control', 'max-age=31536000, public');
            $response->headers->add('Expires', (new \DateTime('UTC + 1 year'))->format('D, d M Y H:i:s \G\M\T'));

            $response->stream = $server->getCache()->readStream($path);

            return $response;
        } catch (\Exception $e) {
            throw new NotSupportedException($e->getMessage());
        }
    }
}
